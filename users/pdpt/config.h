/**
 * - \users\pdpt\config.h ------------------------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  \see <https://docs.qmk.fm/#/config_options?id=the-configh-file>
 * -----------------------------------------------------------------------------
 */

#ifndef USER_CONFIG_H_
#define USER_CONFIG_H_

#define VERBOSE

#define RETRO_TAPPING
#define TAPPING_TERM_PER_KEY
#define TAPPING_TERM  200
#define LAYER_SWITCH_TAPPING_TERM 200
#define HOME_ROW_MODS_TAPPING_TERM 250

// see: <https://precondition.github.io/home-row-mods#retro-tapping>
#define DUMMY_MOD_NEUTRALIZER_KEYCODE KC_F18
// Neutralize left alt and left GUI (Default value)
#define MODS_TO_NEUTRALIZE { MOD_BIT(KC_LEFT_ALT), MOD_BIT(KC_LEFT_GUI) }
// Neutralize left alt, left GUI, right GUI and left Control+Shift
// #define MODS_TO_NEUTRALIZE { MOD_BIT(KC_LEFT_ALT), MOD_BIT(KC_LEFT_GUI), MOD_BIT(KC_RIGHT_GUI), MOD_BIT(KC_LEFT_CTRL)|MOD_BIT(KC_LEFT_SHIFT) }

#ifndef RGB_MATRIX_FRAMEBUFFER_EFFECTS
    #define RGB_MATRIX_FRAMEBUFFER_EFFECTS
#endif

#endif /* USER_CONFIG_H_ */


