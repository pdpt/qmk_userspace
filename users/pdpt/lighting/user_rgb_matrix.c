/**
 * - \users\pdpt\lighting\user_rgb_matrix.c ------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * -----------------------------------------------------------------------------
 */

#include "action_layer.h"
#include "keymap_common.h"
#include "rgb_matrix.h"
#include QMK_KEYBOARD_H
#include "pdpt.h"
#include "pdpt.c"

#include "keys/layer_macros.h"



//// RGB get_bedman_rgb(uint8_t index, uint8_t time){
////     RGB colorA = {};
////     RGB colorB = {};
////     fract8 Aweight = (256-index+time)/256;
////     fract8 Bweight = (index+time )/256;
////     uint8_t red = scale8(colorA.r, Aweight) + scale8(colorB.r, Bweight);
////     RGB output ='';
//// }
#if defined (CUSTOM_RGB_MATRIX)
#ifdef RGB_MATRIX_FRAMEBUFFER_EFFECTS
uint8_t u_rgb_frame_buffer_hue[MATRIX_ROWS][MATRIX_COLS] = {{0}};
#endif // RGB_MATRIX_FRAMEBUFFER_EFFECTS

/**
 *  @see <https://docs.qmk.fm/#/feature_rgb_matrix?id=callbacks>
 * @param    led_min
 * @param    led_max
 * @return   true
 * @return   false
 */
bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    // if this is false, only turn off un-used keys; leaving the default animation playing for the rest.
    bool set_color = FALSE;
    // if this is set to false, leave the un-used keys playing the animation.
    bool turn_off  = true;

    if (get_highest_layer(layer_state) > 0) {
        uint8_t layer = get_highest_layer(layer_state);
        for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
            for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
                uint8_t index = g_led_config.matrix_co[row][col];
                if (index >= led_min && index < led_max && index != NO_LED ){
                    if(keymap_key_to_keycode(layer, (keypos_t){col,row}) > KC_TRNS) {
                        if (set_color) {
                            if(layer == _ADJUST){
                                old_color = rgb_matrix_config.hsv;
                                rgb_matrix_sethsv_noeeprom(HSV_WHITE);
                                // rgb_matrix_set_color(index, RGB_WHITE);
                            } else {
                                rgb_matrix_sethsv_noeeprom(old_color.h, old_color.s, old_color.v);
                                // RGB rgb = hsv_to_rgb(old_color);
                                // rgb_matrix_set_color(index, rgb.r, rWgb.g, rgb.b);
                                // rgb_matrix_set_color(index, RGB_GREEN);
                            }
                        }
                    } else {
                        if (turn_off) {rgb_matrix_set_color(index, RGB_OFF);}
                    }
                }
            }
        }
    }

    //// for (uint8_t row = 0; row < MATRIX_ROWS; ++row) {
    ////     for (uint8_t col = 0; col < MATRIX_COLS; ++col) {
    ////         uint8_t index = g_led_config.matrix_co[row][col];
    ////         if (index >= led_min && index < led_max && index != NO_LED ){
    ////             RGB rgb = get_bedman_rgb(index, time);
    ////             rgb_matrix_set_color(index, rgb.r, rgb.g, rgb.b);
    ////         }
    ////     }
    //// }



    return false;
}

#endif
