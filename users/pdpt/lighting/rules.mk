################################################################################
# ~\qmk_userspace\users\pdpt\lighting\rules.mk                                 #
#                                                                              #
# Copyright 2024 Taylor Premo tpre@pm.me                                       #
# gitlab.com/pdpt                                                              #
# SPDX-License-Identifier: GPL-3.0-or-later                                    #
#                                                                              #
# ===----------------------------------------------------------------------=== #
#                                                                              #
# \see <https://docs.qmk.fm/#/feature_rgb_matrix>                              #
#                                                                              #
# \file                                                                        #
# <<fileoverview>>                                                             #
#                                                                              #
################################################################################

# include feature source files in the relative rules.mk
# only include files for enabled features
CUSTOM_RGB_MATRIX ?= yes
RGB_MATRIX_CUSTOM_USER ?= yes

$(info $$RGB_MATRIX_CUSTOM_USER is [${RGB_MATRIX_CUSTOM_USER}])
$(info $$CUSTOM_RGB_MATRIX is [${CUSTOM_RGB_MATRIX}])
ifeq ($(strip $(RGB_MATRIX_ENABLE)), yes)
    ifeq ($(strip $(CUSTOM_RGB_MATRIX)), yes)
        SRC += lighting/user_rgb_matrix.c \
			lighting/rgb_matrix_naming.c \
			# lighting/rgb_matrix_get_mode_string.c \


        CONFIG_H += lighting/rgb_matrix_config.h
        # POST_CONFIG_H += lighting/post_rgb_matrix_config.h

#         OPT_DEFS += -DCUSTOM_RGB_MATRIX
#         RGB_MATRIX_CUSTOM_USER = yes
    endif
endif
