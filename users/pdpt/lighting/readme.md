# rgb

* Backlight    - bl
* LED matrix   - lm
* rgb lighting - rl
* rgb matrix   - rm

## bedman colors

hsv:  

* pink/body  - 227 108 110
* purple/bed - 221 52 95
* green/hair - 51 15 110
