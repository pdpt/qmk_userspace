/**

 * - \users\pdpt\lighting\lighting_config.h ------------------------------------
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * -----------------------------------------------------------------------------
 */

#ifndef LIGHTING_CONFIG_H_
#define LIGHTING_CONFIG_H_

#include "color.h"


#ifndef BASE_COLOR
    #define BASE_COLOR HSV_GREEN
#endif
#ifndef GAME_COLOR
    #define GAME_COLOR HSV_RED
#endif
#ifndef LOWER_COLOR
    #define LOWER_COLOR HSV_BLUE
#endif
#ifndef RAISE_COLOR
    #define RAISE_COLOR HSV_YELLOW
#endif
#ifndef NAVIGATION_COLOR
    #define NAVIGATION_COLOR HSV_MAGENTA
#endif
#ifndef ADJUST_COLOR
    #define ADJUST_COLOR HSV_WHITE
#endif


#endif /* LIGHTING_CONFIG_H_ */
