// disable all the animations so i can choose the order, and enable only the ones I want.
#ifndef RGB_MATRIX_DISABLE_ANIMATIONS_H_
#define RGB_MATRIX_DISABLE_ANIMATIONS_H_

// #undef ENABLE_RGB_MATRIX_SOLID_COLOR               // 01 Static single hue, no speed support
#undef ENABLE_RGB_MATRIX_ALPHAS_MODS               // 02 Static dual hue, speed is hue for secondary hue

#undef ENABLE_RGB_MATRIX_GRADIENT_UP_DOWN          // 03 Static gradient top to bottom, speed controls how much gradient changes
#undef ENABLE_RGB_MATRIX_GRADIENT_LEFT_RIGHT       // 04 Static gradient left to right, speed controls how much gradient changes

#undef ENABLE_RGB_MATRIX_BREATHING                 // 05 Single hue brightness cycling animation
#undef ENABLE_RGB_MATRIX_BAND_SAT                  // 06 Single hue band fading saturation scrolling left to right
#undef ENABLE_RGB_MATRIX_BAND_VAL                  // 07 Single hue band fading brightness scrolling left to right
#undef ENABLE_RGB_MATRIX_BAND_PINWHEEL_SAT         // 08 Single hue 3 blade spinning pinwheel fades saturation
#undef ENABLE_RGB_MATRIX_BAND_PINWHEEL_VAL         // 09 Single hue 3 blade spinning pinwheel fades brightness
#undef ENABLE_RGB_MATRIX_BAND_SPIRAL_SAT           // 10 Single hue spinning spiral fades saturation
#undef ENABLE_RGB_MATRIX_BAND_SPIRAL_VAL           // 11 Single hue spinning spiral fades brightness
#undef ENABLE_RGB_MATRIX_CYCLE_ALL                 // 12 Full keyboard solid hue cycling through full gradient
#undef ENABLE_RGB_MATRIX_CYCLE_LEFT_RIGHT          // 13 Full gradient scrolling left to right
#undef ENABLE_RGB_MATRIX_CYCLE_UP_DOWN             // 14 Full gradient scrolling top to bottom
#undef ENABLE_RGB_MATRIX_CYCLE_OUT_IN              // 16 Full gradient scrolling out to in
#undef ENABLE_RGB_MATRIX_CYCLE_OUT_IN_DUAL         // 17 Full dual gradients scrolling out to i      g left to right
#undef ENABLE_RGB_MATRIX_CYCLE_PINWHEEL            // 18 Full gradient spinning pinwheel ar   ound center of keyboard
#undef ENABLE_RGB_MATRIX_CYCLE_SPIRAL              // 19 Full gradient spinning spiral around center of keyboard
#undef ENABLE_RGB_MATRIX_DUAL_BEACON               // 20 Full gradient spinning around center of keyboard
#undef ENABLE_RGB_MATRIX_RAINBOW_BEACON            // 21 Full tighter gradient spinning around center of keyboard
#undef ENABLE_RGB_MATRIX_RAINBOW_PINWHEELS         // 22 Full dual gradients spinning two halfs of keyboard
#undef ENABLE_RGB_MATRIX_RAINDROPS                 // 23 Randomly changes a single key's hue
#undef ENABLE_RGB_MATRIX_JELLYBEAN_RAINDROPS       // 24 Randomly changes a single key's hue and saturation
#undef ENABLE_RGB_MATRIX_HUE_BREATHING             // 25 Hue shifts up a slight ammount at the same time, then shifts back
#undef ENABLE_RGB_MATRIX_HUE_PENDULUM              // 26 Hue shifts up a slight ammount in a wave to the right, then back to the left
#undef ENABLE_RGB_MATRIX_HUE_WAVE                  // 27 Hue shifts up a slight ammount and then back down in a wave to the right
#undef ENABLE_RGB_MATRIX_PIXEL_RAIN                // 28 Randomly light keys with random hues
#undef ENABLE_RGB_MATRIX_PIXEL_FRACTAL             // 30 Single hue fractal filled ke ys pulsing horizontally out to edges
#undef ENABLE_RGB_MATRIX_TYPING_HEATMAP            // 31 How hot is your WPM!
#undef ENABLE_RGB_MATRIX_DIGITAL_RAIN              // 32 That famous computer simulation

#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE_SIMPLE     // 33 Pulses keys hit to hue & value then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE            // 34 Static single hue, pulses keys hit to shifted hue then fades to current hue
#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE_WIDE       // 35 Hue & value pulse near a single key hit then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE_MULTIWIDE  // 36 Hue & value pulse near multiple key hits then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE_CROSS      // 37 Hue & value pulse the same column and row of a single key hit then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE_MULTICROSS // 38 Hue & value pulse the same column and row of multiple key hits then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE_NEXUS      // 39 Hue & value pulse away on the same column and row of a single key hit then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_REACTIVE_MULTINEXUS // 40 Hue & value pulse away on the same column and row of multiple key hits then fades value out
#undef ENABLE_RGB_MATRIX_SPLASH                    // 41 Full gradient & value pulse away from a single key hit then fades value out
#undef ENABLE_RGB_MATRIX_MULTISPLASH               // 41 Full gradient & value pulse away from multiple key hits then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_SPLASH              // 43 Hue & value pulse away from a single key hit then fades value out
#undef ENABLE_RGB_MATRIX_SOLID_MULTISPLASH         // 44 Hue & value pulse away from multiple key hits then fades value out

#undef ENABLE_RGB_MATRIX_STARLIGHT                 // 45 LEDs turn on and off at random at varying brightness, maintaining user set color
#undef ENABLE_RGB_MATRIX_STARLIGHT_DUAL_HUE        // 46 LEDs turn on and off at random at varying brightness, modifies user set hue by +- 30
#undef ENABLE_RGB_MATRIX_STARLIGHT_DUAL_SAT        // 47 LEDs turn on and off at random at varying brightness, modifies user set saturation by +- 30
#undef ENABLE_RGB_MATRIX_RIVERFLOW                 // 48 Modification to breathing animation, offset's animation depending on key location to simulate a river flowing
#undef ENABLE_RGB_MATRIX_FLOWER_BLOOMING           // Full tighter gradient of first half scrolling left to right and second half scrolling right to left
#undef ENABLE_RGB_MATRIX_PIXEL_FLOW                // Pulsing RGB flow along LED wiring with random hues

#undef ENABLE_RGB_MATRIX_RAINBOW_MOVING_CHEVRON

#endif /* RGB_MATRIX_DISABLE_ANIMATIONS_H_ */
