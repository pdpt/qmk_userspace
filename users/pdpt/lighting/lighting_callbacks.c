/**
 * - \users\pdpt\lighting\lighting_callbacks.c ---------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * -----------------------------------------------------------------------------
 */

#include "action_layer.h"
#include "lighting_config.h"
#include "pdpt.h"
// #include "pdpt.c"
#include "print.h"

#include QMK_KEYBOARD_H
#include "keys/layer_macros.h"

#if defined (RGB_MATRIX_ENABLE)
    #include "rgb_matrix.h"
    #include "lighting/rgb_matrix_naming.h"
#endif

// #include "quantum/caps_word.h"
// #include "keys/key_macros.h"
#include "quantum/quantum_keycodes.h" // IWYU pragma: keep



void user_sethsv_noeeprom(uint16_t hue, uint8_t sat, uint8_t val){
    #if defined (RGB_MATRIX_ENABLE)
        rgb_matrix_sethsv_noeeprom(hue, sat, val);
    #elif defined (RGBLIGHT_ENABLE)
        rgblight_sethsv_noeeprom(hue, sat, val);
    #elif
        uprintf("NO VALID LIGHTING HSV");
    #endif
}

void user_lighting_mode_noeeprom(uint8_t mode){
    #if defined (RGB_MATRIX_ENABLE)
        rgb_matrix_mode_noeeprom(mode);
    #elif defined (RGBLIGHT_ENABLE)
        rgblight_mode_noeeprom(mode);
    #endif
}

void layer_state_set_user_color(uint8_t layer){
    switch (layer) {
        case _BASE:
            user_sethsv_noeeprom(BASE_COLOR);
            break;
        case _GAME:
            user_sethsv_noeeprom(GAME_COLOR);
            break;
        case _LOWER:
            user_sethsv_noeeprom(LOWER_COLOR);
            break;
        case _RAISE:
            user_sethsv_noeeprom(RAISE_COLOR);
            break;
        case _NAVIGATION:
            user_sethsv_noeeprom(NAVIGATION_COLOR);
            break;
        case _ADJUST:
            user_sethsv_noeeprom(ADJUST_COLOR);
            break;
        default:
            #if defined (CUSTOM_RGB_MATRIX)
                user_sethsv_noeeprom(userspace_rgb_config.hsv.h, userspace_rgb_config.hsv.s, userspace_rgb_config.hsv.v);
            #endif
    }
}

void layer_state_set_user_mode(uint8_t layer){
    switch (layer){
        case _GAME:
            #if defined (RGB_MATRIX_ENABLE)
                rgb_matrix_mode_noeeprom(RGB_MATRIX_STARLIGHT);
            #endif
            break;
        case _BASE:
        default:
            #if defined (RGB_MATRIX_ENABLE)
                rgb_matrix_mode_noeeprom(userspace_rgb_config.mode);
            #endif
    }
}

layer_state_t layer_state_set_user_lighting(layer_state_t state) {
    uint8_t layer = get_highest_layer(state);
    layer_state_set_user_color(layer);
    layer_state_set_user_mode(layer);
    #if defined (RGB_MATRIX_ENABLE)
        uprintf("mode: %s\n", rgb_matrix_name(rgb_matrix_get_mode()));
    #endif

    return state;
}

void keyboard_post_init_user_lighting(void) {
    #if defined (CUSTOM_RGB_MATRIX)
    userspace_rgb_config.hsv  = rgb_matrix_config.hsv;
    userspace_rgb_config.mode = rgb_matrix_config.mode;
    #endif
}
