/**
 * - \users\pdpt\lighting\rgb_matrix_config.h ----------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *
 * \see <https://docs.qmk.fm/#/feature_rgb_matrix?id=additional-configh-options>
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * -----------------------------------------------------------------------------
 */


#ifndef RGB_MATRIX_CONFIG_H_
#define RGB_MATRIX_CONFIG_H_

#include "lighting/rgb_matrix_disable_animations.h" // IWYU pragma: keep

// turn off effects when suspended
#define RGB_MATRIX_SLEEP
// number of milliseconds to wait until rgb automatically turns off
#define RGB_MATRIX_TIMEOUT 1000 * 60 * 5 // 5 minutes

#undef RGB_MATRIX_MAXIMUM_BRIGHTNESS
#define RGB_MATRIX_MAXIMUM_BRIGHTNESS 140 // limits maximum brightness of LEDs to 200 out of 255. If not defined maximum brightness is set to 255

#define RGB_MATRIX_DEFAULT_VAL RGB_MATRIX_MAXIMUM_BRIGHTNESS // Sets the default brightness value, if none has been set

#define RGB_MATRIX_DEFAULT_ON true // Sets the default enabled state, if none has been set

// #define ENABLE_RGB_MATRIX_RIVERFLOW
// #define ENABLE_RGB_MATRIX_TEST
// #define ENABLE_RGB_MATRIX_LED_TRACE
#define ENABLE_RGB_MATRIX_STARLIGHT
// #define ENABLE_RGB_MATRIX_PIXEL_RAIN
// #define ENABLE_RGB_MATRIX_RAINDROPS
// #define ENABLE_RGB_MATRIX_FLOWER_BLOOMING
// #define ENABLE_RGB_MATRIX_PIXEL_FLOW
#define ENABLE_RGB_MATRIX_NEW_RIVER

// #ifndef CUSTOM_RGB_MATRIX
#define CUSTOM_RGB_MATRIX
// #endif

// #define RGB_MATRIX_DEFAULT_MODE RGB_MATRIX_CUSTOM_TEST

#endif /* RGB_MATRIX_CONFIG_H_ */
