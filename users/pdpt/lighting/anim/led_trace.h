#ifdef ENABLE_RGB_MATRIX_LED_TRACE
RGB_MATRIX_EFFECT(LED_TRACE) // no semicolon
#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS
const HSV hsv       = {0, 255, 140};
const uint8_t delay = 100;

// static uint8_t position = 0;
// static uint8_t time = 0;
static RGB rgb;
static void LED_TRACE_INIT(effect_params_t* params){
    rgb_matrix_set_color_all(0, 0, 0);
    rgb = hsv_to_rgb(hsv);
}
//// static bool LED_TRACE_RUN(effect_params_t* params){
////     RGB_MATRIX_USE_LIMITS(led_min, led_max);
////     if (time < delay){
////         time++;
////     } else {
////         time = 0;
////
////         // for (uint8_t i = led_min; i < led_max; i++) {
////         for (uint8_t i = 0; i < RGB_MATRIX_LED_COUNT; i++) {
////             // if (i == position){
////             if (
////                 !(i %  position) &&
////                 !(rand() % 3)
////             ){
////                 rgb_matrix_set_color(i, rgb.r, rgb.g, rgb.b);
////             } else {
////                 rgb_matrix_set_color(i, 0, 0, 0);
////             }
////         }
////         position++;
////         // if(position >= RGB_MATRIX_LED_COUNT){
////         if(position >= MATRIX_ROWS){
////             position = 0;
////         }
////     }
////     return rgb_matrix_check_finished_leds(led_max);
//// }

static bool river_test(effect_params_t* params){
    RGB_MATRIX_USE_LIMITS(led_min, led_max);
    for (uint8_t i = led_min; i < led_max; i++) {
        HSV      hsv  = rgb_matrix_config.hsv;
        uint16_t time = scale16by8(g_rgb_timer + ((i + rand() % 2) * 315), rgb_matrix_config.speed / 8);
        // uint8_t rand_offset = rand() % 10;
        hsv.v         = scale8(abs8(sin8(time) - 128) * 2, hsv.v);
        RGB rgb       = rgb_matrix_hsv_to_rgb(hsv);
        RGB_MATRIX_TEST_LED_FLAGS();
        rgb_matrix_set_color(i, rgb.r, rgb.g, rgb.b);
    }

    return rgb_matrix_check_finished_leds(led_max);
}
static bool LED_TRACE(effect_params_t* params){
    if (params->init){LED_TRACE_INIT(params);}
    return river_test(params);
}

#endif // RGB_MATRIX_CUSTOM_EFFECT_IMPLS
#endif // ENABLE_RGB_MATRIX_LED_TRACE
