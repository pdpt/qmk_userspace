#ifdef ENABLE_RGB_MATRIX_RAINBOW_DIGITAL_RAIN
#if defined(RGB_MATRIX_FRAMEBUFFER_EFFECTS)
RGB_MATRIX_EFFECT(RAINBOW_DIGITAL_RAIN) // no semicolon
#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS
    /**
     * ~ CONFIG VARIABLES
     *
     * * DECAY_MS                 - ms (milliseconds) for a drop to fully fade
     * * DROP_MS                  - ms between each 'drop' tick.
     * * NEW_DROP_PROBABILITY     - 1/n odds of a new raindrop each `DROP_MS` tick
     * * PURE_INTENSITY           - 'intensity' level for max saturation
     * * MAXIMUM_BRIGHTNESS_BOOST - 'boost' is value added to secondary colors to increase brightness
     * * MAXIMUM_BRIGHTNESS       - maximum allowable brightness, incase we want it less than 255
     * * COLOR_CHANNEL            - red, green, or blue
     */

	// #define DECAY_MS                 2000
	#define DROP_MS                  45
	#define NEW_DROP_PROBABILITY     15
	// #define PURE_INTENSITY           0xd0
	// #define MAXIMUM_BRIGHTNESS_BOOST 0xc0
	#define MAXIMUM_BRIGHTNESS       0xff
	// #define COLOR_CHANNEL            1
    #define decayAmount 2


    /**
     *  ~ INTERNAL VARIABLES
     *
     * * dropStartTimestamp        - timer at start of drop cycle
     * * previousTimestamp         - timer at previous tick
     * * justDropped               - if a raindrops fell last tick
     * * MATRIX_COLS               - number of LED columns
     * * MATRIX_ROWS               - number of LED rows
     * * LED_INTENSITY[COLS][ROWS] - array of intensities for each LED
     * * LED_HUE[COLS][ROWS]       - array of hues for each LED
     */
    // static uint32_t dropStartTimestamp = 0;
    // static uint32_t previousTimestamp  = 0;
    static bool justDropped   = false;
    static uint8_t drop_timer = 0;

    static uint8_t LED_INTENSITY[MATRIX_COLS][MATRIX_ROWS] = {{0}};
    static uint8_t       LED_HUE[MATRIX_COLS][MATRIX_ROWS] = {{0}};

    static void RAINBOW_DIGITAL_RAIN_INIT(effect_params_t* params){
        rgb_matrix_set_color_all(0, 0, 0);
        memset(LED_INTENSITY, 0, sizeof(LED_INTENSITY));
        memset(LED_HUE, 0, sizeof(LED_HUE));
        drop_timer = 0;
    }

    static RGB getRGB(uint8_t intensity, uint8_t hue){
        HSV hsv = {hue, 255, intensity};
        return hsv_to_rgb(hsv);
    }

    static void DECAY_AND_NEW(void){
        uint8_t col;
		uint8_t row;

        for (col = 0; col < MATRIX_COLS; col++) {
			for (row = 0; row < MATRIX_ROWS; row++) {
				if (
                    row == 0 &&                                     // This is the top row,
                    justDropped &&                                  // pixels have just fallen,
                    !(rand() % NEW_DROP_PROBABILITY)                // and we've decided to make a new raindrop in this column
                ) {
                    LED_INTENSITY[col][row] = MAXIMUM_BRIGHTNESS;   // set to max brightness
                          LED_HUE[col][row] = rand() % 255;         // pick a random hue

				} else if (
                    LED_INTENSITY[col][row] > 0 &&                  // Pixel is neither full brightness
                    LED_INTENSITY[col][row] < MAXIMUM_BRIGHTNESS    // nor totally dark;
                ) {                                                 // decay it
					if (LED_INTENSITY[col][row] <= decayAmount) {
						LED_INTENSITY[col][row] = 0;                // don't decay below 0
					} else {
						LED_INTENSITY[col][row] -= decayAmount;
					}
				}

				// Set the color for this pixel
                uint8_t intensity = LED_INTENSITY[col][row];
                uint8_t hue       = LED_HUE[col][row];
                RGB     rgb       = getRGB(intensity, hue);
                uint8_t led[LED_HITS_TO_REMEMBER];
                uint8_t led_count = rgb_matrix_map_row_column_to_led(row, col, led);
                uint8_t index     = led[0];

                if (led_count > 0) {
                    rgb_matrix_set_color(index, rgb.r, rgb.g, rgb.b);
                }
			}
		}
    }

    static void DROP_RAINDROPS(void){
        uint8_t col;
		uint8_t row;

        for (row = MATRIX_ROWS - 1; row > 0; row--) {
            for (col = 0; col < MATRIX_COLS; col++) {
                if (
                    row == MATRIX_ROWS - 1 &&                               // If this pixel is on the bottom row
                    LED_INTENSITY[col][row] == MAXIMUM_BRIGHTNESS           // and bright,
                ) {
                    LED_INTENSITY[col][row]--;                              // allow it to start decaying
                }
                if (LED_INTENSITY[col][row - 1] == MAXIMUM_BRIGHTNESS) {    // Check if the pixel above is bright
                    LED_INTENSITY[col][row - 1]--;                          // Allow old bright pixel to decay
                    LED_INTENSITY[col][row] = MAXIMUM_BRIGHTNESS;           // Make this pixel bright
                    LED_HUE[col][row] = LED_HUE[col][row-1];                // copy the hue from above
                }
            }
        }
    }

    static bool RAINBOW_DIGITAL_RAIN_RUN(effect_params_t* params) {
        // Decay intensities and possibly make new raindrops
		DECAY_AND_NEW();

        // drop raindrops one row periodically
        if(drop_timer > DROP_MS){
            drop_timer = 0;
            justDropped = true;
            DROP_RAINDROPS();
        } else {
            justDropped = false;
        }

        drop_timer++;

        return false;
    }
    static bool RAINBOW_DIGITAL_RAIN(effect_params_t* params) {
        if (params->init){RAINBOW_DIGITAL_RAIN_INIT(params);}
        return RAINBOW_DIGITAL_RAIN_RUN(params);
    }


#endif // RGB_MATRIX_CUSTOM_EFFECT_IMPLS
#endif // defined(RGB_MATRIX_FRAMEBUFFER_EFFECTS)
#endif // ENABLE_RGB_MATRIX_RAINBOW_DIGITAL_RAIN
