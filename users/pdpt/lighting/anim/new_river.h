/**
 * ~ \users\pdpt\lighting\anim\new_river.h -------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  combines river, pixel flow, and pixel hue to give a more random value.
 * * river value - <https://github.com/qmk/qmk_firmware/blob/master/quantum/rgb_matrix/animations/riverflow_anim.h>
 * * pixel flow  - <https://github.com/qmk/qmk_firmware/blob/master/quantum/rgb_matrix/animations/pixel_flow_anim.h>
 * * pixel hue   - <https://github.com/qmk/qmk_firmware/blob/master/quantum/rgb_matrix/animations/pixel_hue_anim.h>
 * -----------------------------------------------------------------------------
 */

#ifdef ENABLE_RGB_MATRIX_NEW_RIVER
RGB_MATRIX_EFFECT(NEW_RIVER)
#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS

/**
 * ~ CONFIG VARIABLES
 */
const uint8_t  max_hue_shift     = 20;
const uint8_t  max_value_blend   = 150;
const uint8_t  min_brightness    = 10;
const uint16_t max_time_offset   = 400;
const uint32_t value_update_rate = 10000;  // 3000
const uint32_t hue_update_rate   = 3000;   // 500
/**
 *  ~ INTERNAL VARIABLES
 *
 * * value_array & hue_array - arrays of random values [0..2] based on the logic of pixel value/hue
 * * value_timer & hue_timer - timers for when to update the pixel value/hue arrays
 */

static uint8_t  value_array[RGB_MATRIX_LED_COUNT];
static uint8_t  hue_array[RGB_MATRIX_LED_COUNT];
static uint16_t time_array[RGB_MATRIX_LED_COUNT];
static uint32_t value_timer = 0;
static uint32_t hue_timer   = 0;

static uint32_t value_interval(void) {
    return value_update_rate / scale16by8(qadd8(rgb_matrix_config.speed, 16), 16);
}

static uint32_t hue_interval(void) {
    return hue_update_rate / scale16by8(qadd8(rgb_matrix_config.speed, 16), 16);
}

static uint8_t get_value(uint8_t index){
    // river value
    uint8_t  value = RGB_MATRIX_MAXIMUM_BRIGHTNESS;

    uint16_t time  = scale16by8(g_rgb_timer + ((index) * 315)+time_array[index]-max_time_offset, rgb_matrix_config.speed / 8);
             value = scale8(abs8(sin8(time) - 128) * 2, value);

    uint8_t blend = value_array[index];
    // adjust by hue
    if(blend < (max_value_blend/2)){
        value = blend8(value, min_brightness, (max_value_blend - blend));
    } else {
        value = blend8(value, RGB_MATRIX_MAXIMUM_BRIGHTNESS, blend);
    }
    return value;
}

static uint8_t get_hue(uint8_t index){
    uint8_t hue = rgb_matrix_config.hsv.h;
    uint8_t shift = hue_array[index];
    if(shift < (max_hue_shift/2)){
        hue = submod8(hue, shift, 255);
    } else {
        hue = addmod8(hue, (shift - (max_hue_shift/2)), 255);
    }

    return hue;
}

static void NEW_RIVER_VALUE_UPDATE(void){

    uint8_t index = random8_max(RGB_MATRIX_LED_COUNT);
    value_array[index] = random8() % max_value_blend;
    value_timer = g_rgb_timer + value_interval();
}

static void NEW_RIVER_HUE_UPDATE(void){
    uint8_t index = random8_max(RGB_MATRIX_LED_COUNT);
    hue_array[index] = random8() % max_hue_shift;
    hue_timer = g_rgb_timer + hue_interval();
}

static void NEW_RIVER_INIT(void){
    // clear LEDs
    rgb_matrix_set_color_all(0, 0, 0);
    // fill value_array
    for (uint8_t j = 0; j < RGB_MATRIX_LED_COUNT; ++j) {
        value_array[j] = random8() % max_value_blend;
        hue_array[j]   = random8() % max_hue_shift;
        time_array[j]  = random8() % max_time_offset;
    }
}

static bool NEW_RIVER_RUN(void) {
    uint8_t col;
    uint8_t row;

    for (col = 0; col < MATRIX_COLS; col++) {
        for (row = 0; row < MATRIX_ROWS; row++) {

            uint8_t led[LED_HITS_TO_REMEMBER];
            uint8_t led_count = rgb_matrix_map_row_column_to_led(row, col, led);
            uint8_t index     = led[0];

            HSV hsv = rgb_matrix_config.hsv;
            hsv.v = get_value(index);
            hsv.h = get_hue(index);

            RGB rgb = hsv_to_rgb(hsv);

            if (led_count > 0) {
                rgb_matrix_set_color(index, rgb.r, rgb.g, rgb.b);
            }
        }
    }

    return false;
}

static bool NEW_RIVER(effect_params_t* params) {
    if (params -> init){ NEW_RIVER_INIT(); }

    if (!(value_timer > g_rgb_timer)) {
        // uprintf("value update\n");
        NEW_RIVER_VALUE_UPDATE();
    }

    if (g_rgb_timer > hue_timer) {
        // uprintf("hue update\n");
        NEW_RIVER_HUE_UPDATE();
    }

    NEW_RIVER_RUN();
    return false;
}

#endif // RGB_MATRIX_CUSTOM_EFFECT_IMPLS
#endif // ENABLE_RGB_MATRIX_NEW_RIVER
