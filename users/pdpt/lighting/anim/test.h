// !!! DO NOT ADD #pragma once !!! //

#ifdef ENABLE_RGB_MATRIX_TEST
RGB_MATRIX_EFFECT(TEST) //note the lack of semicolon after the macro!
#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS

#define RED   100,   0,   0
#define GREEN   0, 100,   0
#define BLUE    0,   0, 100
#define OFF     0,   0,   0

#define getRow(i) i / 15
#define getCol(i) i % 15

bool didInit = false;

// e.g: A simple effect, self-contained within a single method
static bool TEST(effect_params_t* params) {
    RGB_MATRIX_USE_LIMITS(led_min, led_max);

    if (params->init) {
        if (!didInit){
            uprintf("rgb matrix mode : %s\n", __func__);
            didInit = true;
        }
    } else {
        didInit = false;
    }

    for (uint8_t i = led_min; i < led_max; i++) {
        if (i % 5 == 0) {
            rgb_matrix_set_color(i, RED);
        } else if (getRow(i) % 2 == 0) {
            rgb_matrix_set_color(i, BLUE);
        } else {
            rgb_matrix_set_color(i, OFF);
        }
    }
    return rgb_matrix_check_finished_leds(led_max);
}

#endif // RGB_MATRIX_CUSTOM_EFFECT_IMPLS
#endif // ENABLE_RGB_MATRIX_TEST
