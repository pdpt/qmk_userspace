# pdpt/lighting/anim

contains custom LED Matrix Effects.

see: <https://docs.qmk.fm/#/feature_led_matrix?id=custom-led-matrix-effects>

## setup

* set `RGB_MATRIX_CUSTOM_USER` in `rules.mk`
* create `led_matrix_user.inc` in userspace directory.
  * include animation `.h` files here
* custom effects are prepended with `RGB_MATRIX_CUSTOM_`
  * i.e. `RGB_MATRIX_EFFECT(my_cool_effect)` creates the matrix mode `RGB_MATRIX_CUSTOM_my_cool_effect`

## file structure

`animation.h`
* do not add `#pragma once` or equivalent
* `RGB_MATRIX_EFFECT(ANIM_NAME)`
* surround everything else in `#ifdef RGB_MATRIX_CUSTOM_EFFECT_IMPLS` block
* see: <https://github.com/qmk/qmk_firmware/tree/master/quantum/led_matrix/animations> for examples
