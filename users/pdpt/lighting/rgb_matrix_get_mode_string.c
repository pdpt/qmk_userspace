#include "rgb_matrix.h"
#include "lighting/rgb_matrix_get_mode_string.h"
#include "lighting/rgb_matrix_config.h" // IWYU pragma: keep


#define rgb_case(x) case x: return #x
char* rgb_matrix_get_mode_string(void) {

    switch (rgb_matrix_get_mode()) {
        rgb_case(RGB_MATRIX_SOLID_COLOR);    // solid color is always enabled
    #if   defined RGB_MATRIX_ALPHAS_MODS
        rgb_case(RGB_MATRIX_ALPHAS_MODS);
    #elif defined ENABLE_RGB_MATRIX_GRADIENT_UP_DOWN
        rgb_case(RGB_MATRIX_GRADIENT_UP_DOWN);
    #elif defined ENABLE_RGB_MATRIX_GRADIENT_LEFT_RIGHT
        rgb_case(RGB_MATRIX_GRADIENT_LEFT_RIGHT);
    #elif defined ENABLE_RGB_MATRIX_BREATHING
        rgb_case(RGB_MATRIX_BREATHING);
    #elif defined ENABLE_RGB_MATRIX_BAND_SAT
        rgb_case(RGB_MATRIX_BAND_SAT);
    #elif defined ENABLE_RGB_MATRIX_BAND_VAL
        rgb_case(RGB_MATRIX_BAND_VAL);
    #elif defined ENABLE_RGB_MATRIX_BAND_PINWHEEL_SAT
        rgb_case(RGB_MATRIX_BAND_PINWHEEL_SAT);
    #elif defined ENABLE_RGB_MATRIX_BAND_PINWHEEL_VAL
        rgb_case(RGB_MATRIX_BAND_PINWHEEL_VAL);
    #elif defined ENABLE_RGB_MATRIX_BAND_SPIRAL_SAT
        rgb_case(RGB_MATRIX_BAND_SPIRAL_SAT);
    #elif defined ENABLE_RGB_MATRIX_BAND_SPIRAL_VAL
        rgb_case(RGB_MATRIX_BAND_SPIRAL_VAL);
    #elif defined ENABLE_RGB_MATRIX_CYCLE_ALL
        rgb_case(RGB_MATRIX_CYCLE_ALL);
    #elif defined ENABLE_RGB_MATRIX_CYCLE_LEFT_RIGHT
        rgb_case(RGB_MATRIX_CYCLE_LEFT_RIGHT);
    #elif defined ENABLE_RGB_MATRIX_CYCLE_UP_DOWN
        rgb_case(RGB_MATRIX_CYCLE_UP_DOWN);
    #elif defined ENABLE_RGB_MATRIX_CYCLE_OUT_IN
        rgb_case(RGB_MATRIX_CYCLE_OUT_IN);
    #elif defined ENABLE_RGB_MATRIX_CYCLE_OUT_IN_DUAL
        rgb_case(RGB_MATRIX_CYCLE_OUT_IN_DUAL);
    #elif defined ENABLE_RGB_MATRIX_CYCLE_PINWHEEL
        rgb_case(RGB_MATRIX_CYCLE_PINWHEEL);
    #elif defined ENABLE_RGB_MATRIX_CYCLE_SPIRAL
        rgb_case(RGB_MATRIX_CYCLE_SPIRAL);
    #elif defined ENABLE_RGB_MATRIX_DUAL_BEACON
        rgb_case(RGB_MATRIX_DUAL_BEACON);
    #elif defined ENABLE_RGB_MATRIX_RAINBOW_BEACON
        rgb_case(RGB_MATRIX_RAINBOW_BEACON);
    #elif defined ENABLE_RGB_MATRIX_RAINBOW_PINWHEELS
        rgb_case(RGB_MATRIX_RAINBOW_PINWHEELS);
    #elif defined ENABLE_RGB_MATRIX_RAINDROPS
        rgb_case(RGB_MATRIX_RAINDROPS);
    #elif defined ENABLE_RGB_MATRIX_JELLYBEAN_RAINDROPS
        rgb_case(RGB_MATRIX_JELLYBEAN_RAINDROPS);
    #elif defined ENABLE_RGB_MATRIX_HUE_BREATHING
        rgb_case(RGB_MATRIX_HUE_BREATHING);
    #elif defined ENABLE_RGB_MATRIX_HUE_PENDULUM
        rgb_case(RGB_MATRIX_HUE_PENDULUM);
    #elif defined ENABLE_RGB_MATRIX_HUE_WAVE
        rgb_case(RGB_MATRIX_HUE_WAVE);
    #elif defined ENABLE_RGB_MATRIX_PIXEL_RAIN
        rgb_case(RGB_MATRIX_PIXEL_RAIN);
    #elif defined ENABLE_RGB_MATRIX_PIXEL_FRACTAL
        rgb_case(RGB_MATRIX_PIXEL_FRACTAL);
    #elif defined ENABLE_RGB_MATRIX_TYPING_HEATMAP
        rgb_case(RGB_MATRIX_TYPING_HEATMAP);
    #elif defined ENABLE_RGB_MATRIX_DIGITAL_RAIN
        rgb_case(RGB_MATRIX_DIGITAL_RAIN);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE_SIMPLE
        rgb_case(RGB_MATRIX_SOLID_REACTIVE_SIMPLE);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE
        rgb_case(RGB_MATRIX_SOLID_REACTIVE);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE_WIDE
        rgb_case(RGB_MATRIX_SOLID_REACTIVE_WIDE);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE_MULTIWIDE
        rgb_case(RGB_MATRIX_SOLID_REACTIVE_MULTIWIDE);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE_CROSS
        rgb_case(RGB_MATRIX_SOLID_REACTIVE_CROSS);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE_MULTICROSS
        rgb_case(RGB_MATRIX_SOLID_REACTIVE_MULTICROSS);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE_NEXUS
        rgb_case(RGB_MATRIX_SOLID_REACTIVE_NEXUS);
    #elif defined ENABLE_RGB_MATRIX_SOLID_REACTIVE_MULTINEXUS
        rgb_case(RGB_MATRIX_SOLID_REACTIVE_MULTINEXUS);
    #elif defined ENABLE_RGB_MATRIX_SPLASH
        rgb_case(RGB_MATRIX_SPLASH);
    #elif defined ENABLE_RGB_MATRIX_MULTISPLASH
        rgb_case(RGB_MATRIX_MULTISPLASH);
    #elif defined ENABLE_RGB_MATRIX_SOLID_SPLASH
        rgb_case(RGB_MATRIX_SOLID_SPLASH);
    #elif defined ENABLE_RGB_MATRIX_SOLID_MULTISPLASH
        rgb_case(RGB_MATRIX_SOLID_MULTISPLASH);
    #elif defined ENABLE_RGB_MATRIX_STARLIGHT
        rgb_case(RGB_MATRIX_STARLIGHT);
    #elif defined ENABLE_RGB_MATRIX_STARLIGHT_DUAL_HUE
        rgb_case(RGB_MATRIX_STARLIGHT_DUAL_HUE);
    #elif defined ENABLE_RGB_MATRIX_STARLIGHT_DUAL_SAT
        rgb_case(RGB_MATRIX_STARLIGHT_DUAL_SAT);
    #elif defined ENABLE_RGB_MATRIX_RIVERFLOW
        rgb_case(RGB_MATRIX_RIVERFLOW);
    #elif defined ENABLE_RGB_MATRIX_FLOWER_BLOOMING
        rgb_case(RGB_MATRIX_FLOWER_BLOOMING);
    #elif defined ENABLE_RGB_MATRIX_PIXEL_FLOW
        rgb_case(RGB_MATRIX_PIXEL_FLOW);
    #elif defined ENABLE_RGB_MATRIX_RAINBOW_MOVING_CHEVRON
        rgb_case(RGB_MATRIX_RAINBOW_MOVING_CHEVRON);
    #elif defined ENABLE_RGB_MATRIX_CUSTOM_TEST
        uprint("test defined");
        rgb_case(RGB_MATRIX_CUSTOM_TEST);
    #endif
    default: return "unknown";
    }
}

