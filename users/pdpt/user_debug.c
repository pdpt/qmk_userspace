/**
 * - \users\pdpt\user_debug.c --------------------------------------------------
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * -----------------------------------------------------------------------------
 */

#include "user_debug.h"

#include "action_layer.h"
#include "keys/layer_macros.h"
#include "print.h"

void print_layer(layer_state_t state) {
    switch (get_highest_layer(state)) {
        case _GAME:
            uprintf("layer is: _GAME\n");
            break;
        case _LOWER:
            uprintf("layer is: _LOWER\n");
            break;
        case _RAISE:
            uprintf("layer is: _RAISE\n");
            break;
        case _NUMPAD:
            uprintf("layer is: _NUMPAD\n");
            break;
        case _NAVIGATION:
            uprintf("layer is: _NAVIGATION\n");
            break;
        case _ADJUST:
            uprintf("layer is: _ADJUST\n");
            break;
        case _BASE:
            uprintf("layer is: _BASE\n");
            break;
        default:
            uprintf("layer is unknown!\n");
    }
}
