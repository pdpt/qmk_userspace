/**
 * - \users\pdpt\pdpt.h --------------------------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * -----------------------------------------------------------------------------
 */

#ifndef PDPT_H_
#define PDPT_H_
#include QMK_KEYBOARD_H

#if defined (CUSTOM_RGB_MATRIX)
    #include "rgb_matrix_types.h"
    extern rgb_config_t userspace_rgb_config;
    HSV old_color;
#endif

#endif /* PDPT_H_ */
