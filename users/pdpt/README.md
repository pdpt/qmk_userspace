# PDPT Userspace

see: <https://docs.qmk.fm/#/feature_userspace>
see: <https://github.com/qmk/qmk_firmware/tree/master/users/_example>
see: <https://github.com/drashna/qmk_userspace/tree/master/users/drashna>

todo:

* add documentation references for files/features
* code coverage
* better `;` location
* caps word
* enable & write some debugging.
* look drashna's achordion
* per key tap delay
* better backsp/del options, one-hand with left or right.
* prefix variables with u_

## files

```tree
pdpt/                   //
├─ keys/                //
│  ├─ key_macros.h      // macros for individual keys
│  ├─ key_overrides.c   // custom key behavior goes here
│  └─ layer_macros.h    // macros for entire layers or groups of keys
│                       //
├─ lighting/            // anything related to rgb goes here
│                       //
├─ callbacks.c          //
└─ README.md            //
```

### Rules.mk

This is how you add additional source files for compiling.

```mk
SRC += <name>.c
```

Example: If you have RGB control features shared between all your keyboards that support RGB lighting, you can add support for that if the RGBLIGHT feature is enabled.

```mk
ifeq ($(strip $(RGBLIGHT_ENABLE)), yes)
  # Include my fancy rgb functions source here
  SRC += cool_rgb_stuff.c
endif
```

### config.h

Will be processed like the keymap equivalent. This is handled separately from `<name>.h`

note: `<name>.h` won't be added in time for settings such as `#define TAPPING_TERM 100`.
warning: including `<name>.h` in any `config.h` file will result in compile issues.

## Customized Functions

QMK callback functions are structured as a hierarchy

* Core `_quantum`
  * Keyboard `_kb`
    * keymap `_user` (drashna uses this for userspace)
      * keymap `_keymap` (from drashna)

see: <https://github.com/drashna/qmk_userspace/blob/master/users/drashna/template.c>

```c
__attribute__ ((weak))
layer_state_t layer_state_set_keymap (layer_state_t state) {
  return state;
}

layer_state_t layer_state_set_user (layer_state_t state) {
  state = update_tri_layer_state(state, 2, 3, 5);
  return layer_state_set_keymap (state);
}
```

`__attribute__ ((weak))` allows the function to be re-written in `keymap.c`.

## QMK Features

I hope to organize the userspace by subfolders grouping different features (such as lighting). I am unsure exactly what groupings I want, so I will list them here for now.

* Lighting
  * Backlight
  * LED Matrix
  * RGB Lighting
  * RGB Matrix
* Hardware Features
  * Display
  * Audio
  * Bluetooth
* Layers
  * Tri-layer
* keypress -> firmware effect
  * Combos // press physical keyboard to do something mundane
    * leader
  * Key Override
  * Tap dance
  * tap-hold
    * mod-tap
      * home row mods
* firmware -> PC
  * Caps Word // press something mundane for non-mundane result.
  * Key Lock
  * special keys
    * grave esc
    * space cadet
* firmware internal only
  * EEPROM // firmware internal management
  * deferred  execution
  * housekeeping
  * quantum keycodes
  * magic keycodes (depreciated)
    * Command
* One Shot
* Swap Hands
* macros
  * dynamic macros
  * compiled macros
* unicode
* bootmagic (lite?)

The wiki groups as:

* keycodes
  * command
  * macros
    * dynamic macros
  * multi-function keys
    * grave escape
    * space cadet shift
  * mouse keys
  * programmable button
  * repeat key
  * ANSI Shifted keys
* software
  * Caps Word
  * EEPROM
  * Key Lock
  * Key Overrides
  * Layers
    * tri-layer
  * One Shot
  * unicode
* hardware
  * Displays
  * lighting
  * audio
  * bluetooth
  * bootmagic lite

mod-tap is in keycodes, while tapping term is in software. This makes grouping unclear.

//// moving all 'tap' related features seems to even things out a bit.
////
//// *tapping group
////* mod-tap
////   *tap-hold config
////* tap dance
////   *combos?
////* auto shift
////   * leader key?

mod-tap is a keycode, tapping term is a setting. settings go in `rules.mk` or `config.h` files. these can be grouped/organized separately.

there are also the functions listed: <https://docs.qmk.fm/#/custom_quantum_functions>

## License

Copyright 2024 Taylor Premo <tpre@pm.me>

GPL-3.0-or-later
