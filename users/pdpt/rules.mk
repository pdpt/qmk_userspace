# - \users\pdpt\rules.mk ----------------------------------------------------- #
#  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt                      #
#  SPDX-License-Identifier: GPL-3.0-or-later                                   #
# ---------------------------------------------------------------------------- #
#                                                                              #
# see <https://docs.qmk.fm/#/config_options?id=the-rulesmk-file>               #
#                                                                              #
# <<fileoverview>>                                                             #
#                                                                              #
# ---------------------------------------------------------------------------- #
#                                                                              #
# Simple assignment      `:=`	                                               #
# Recursive assignment    `=`	                                               #
# Conditional assignment `?=`	                                               #
# Appending              `+=`	                                               #
#                                                                              #
# `-inclue` won't generate an error if the file does not exist                 #
################################################################################

# $(info $$[${__DATE__}] [${__TIME__}])

# * for some reason `$(USER_PATH)/pdpt.c` causes error.
SRC += pdpt.c \
	user_debug.c \
	callbacks.c \
	keys/key_overrides.c \
	# keyrecords/process_records.c \
	# keyrecords/tapping.c \
	# eeconfig_users.c

# This makes the process take longer, but it can significantly reduce the compiled size
LTO_ENABLE ?= yes

# for easy adjustment of tapping term, should not be permanently enabled.
# DYNAMIC_TAPPING_TERM_ENABLE ?= yes

# debugging console
CONSOLE_ENABLE ?= yes

# COMMAND_ENABLE = yes

# ============================================================================ #
#                                FEATURE OPTIONS                               #
# ============================================================================ #

# see: <https://docs.qmk.fm/#/config_options?id=feature-options>
# KEY_OVERRIDE_ENABLE ?= yes

# ============================================================================ #
#                                OTHER RULES.MK                                #
# these contain specific rules & link to their respective `config.h` files     #
# * these require `$(USER_PATH)`											   #
# ============================================================================ #

# include $(USER_PATH)/oled/rules.mk
# include $(USER_PATH)/pointing/rules.mk
# include $(USER_PATH)/split/rules.mk
# include $(USER_PATH)/painter/rules.mk
include $(USER_PATH)/lighting/rules.mk
include $(USER_PATH)/keys/rules.mk
# include $(USER_PATH)/features/rules.mk
# Ignore if not found
-include $(KEYMAP_PATH)/post_rules.mk
