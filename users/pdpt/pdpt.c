/**
 * - \users\pdpt\pdpt.c --------------------------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  \see <https://docs.qmk.fm/#/feature_userspace>
 *
 *  \file
 *  used to hold arbitrary custom functions that do not belong anywhere else.
 *  imo should be pretty sparse, in favor of organizing functions based on feature.
 * -----------------------------------------------------------------------------
 */

#if defined (CUSTOM_RGB_MATRIX)
    #include "rgb_matrix_types.h"
    rgb_config_t userspace_rgb_config;
    HSV old_color;
#endif
