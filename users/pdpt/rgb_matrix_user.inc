//===----------------------------------------------------------------------===//
/// ~\users\pdpt\lighting\rgb_matrix_effects\color_gradient_anim.h
///
/// Copyright 2024 Taylor Premo tpre@pm.me
/// gitlab.com/pdpt
/// SPDX-License-Identifier: GPL-3.0-or-later
///
/// ===----------------------------------------------------------------------===
///
/// \see <https://docs.qmk.fm/#/feature_rgb_matrix?id=custom-rgb-matrix-effects>
/// \see <https://github.com/qmk/qmk_firmware/tree/master/quantum/rgb_matrix/animations>
///
/// \file
/// <<fileoverview>>
///
//===----------------------------------------------------------------------===//
#include "lighting/anim/test.h"
#include "lighting/anim/rainbow_digital_rain.h"
#include "lighting/anim/led_trace.h"
#include "lighting/anim/new_river.h"
// #include "solid_color_anim.h"
// #include "riverflow_anim.h"
// #include "raindrops_anim.h"
// #include "jellybean_raindrops_anim.h"

// ~drashna
//// #include "lighting/anim/drashna/cool_diagonal.h" // neaat
//// #include "lighting/anim/drashna/random_breath_rainbow.h" // rgb vomit!
//// #include "lighting/anim/drashna/raindrops.h"
//// #include "lighting/anim/drashna/candy.h"
