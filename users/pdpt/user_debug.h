/**
 * - \users\pdpt\user_debug.h --------------------------------------------------
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * -----------------------------------------------------------------------------
 */

#ifndef USER_DEBUG_H_
#define USER_DEBUG_H_
#include "action_layer.h"  // IWYU pragma: keep

void print_layer(layer_state_t state);


#endif /* USER_DEBUG_H_ */
