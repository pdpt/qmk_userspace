/**
 * - \users\pdpt\callbacks.c ---------------------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  \file
 * single location for QMK callbacks.
 * currently doing it this way cause that's how drashna's is set up.
 * will try to organize differently in the future.
 * see: <https://github.com/drashna/qmk_userspace/blob/master/docs/callbacks.md>
 * see: <https://github.com/drashna/qmk_userspace/blob/master/users/drashna/callbacks.c>
 * -----------------------------------------------------------------------------
 */

#include "action_layer.h"
#include "pdpt.h"
#include "print.h"
#include "user_debug.h"

#include QMK_KEYBOARD_H
#include "keys/layer_macros.h"

#include "quantum/caps_word.h"
#include "keys/key_macros.h"
#include "quantum/quantum_keycodes.h" // IWYU pragma: keep

// #ifdef RGB_MATRIX_ENABLE
#if defined (RGB_MATRIX_ENABLE) || defined (RGBLIGHT_ENABLE)
#include "lighting/lighting_callbacks.c"
#endif



/**
 * @brief This runs code every time that the layers get changed. This can be
 * useful for layer indication, or custom layer handling.
 *
 * @note the rgb on layer stuff seems to not work well with rgb_matrix
 * ! layer enum is in layer_macros.h
 *
 * @param state
 * @return layer_state_t
 */
layer_state_t layer_state_set_user(layer_state_t state) {
    state = update_tri_layer_state(state, _LOWER, _RAISE, _ADJUST);
    #ifdef CONSOLE_ENABLE
        print_layer(state);
    #endif
    #if defined (RGB_MATRIX_ENABLE) || defined (RGBLIGHT_ENABLE)
        state  = layer_state_set_user_lighting(state);
    #endif
    return state;
}

void keyboard_post_init_user(void) {
    // Customise these values to desired behaviour
    debug_enable=true;
    uprintf("π[%s][%s]\n",__DATE__, __TIME__);
    // debug_matrix=true;
    // debug_keyboard=true;
    // debug_mouse=true;
    // rgb_config_t userspace_rgb_config = {0};
    // old_color = rgb_matrix_config.hsv;
    #if defined (RGB_MATRIX_ENABLE)
        keyboard_post_init_user_lighting();
    #endif
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    // If console is enabled, it will print the matrix position and status of each key pressed
    #ifdef CONSOLE_ENABLE
        #if defined (CUSTOM_RGB_MATRIX)
            uprintln("CUSTOM_RGB_MATRIX ON");
        #else
            uprintln("CUSTOM_RGB_MATRIX OFF");
        #endif
        uprintf("KeyLocation: keycode: 0x%04X, col: %2u, row: %2u, pressed: %u, time: %5u, interrupted: %u,  count: %u\n",
            keycode,
            record->event.key.col,
            record->event.key.row,
            record->event.pressed,
            record->event.time,
            record->tap.interrupted,
            record->tap.count
        );
    #endif
    switch (keycode) {
        case CW_NAV:
            if (record->event.pressed){
                if (record->tap.count) {
                    caps_word_toggle();
                    uprintf("is caps word on: %d \n", is_caps_word_on());
                    return false;
                }
            }
            break;
    }
    return true;
}
