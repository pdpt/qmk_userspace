/**
 * - \users\pdpt\keys\key_macros.h ---------------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  defines shorter names for individual keys
 * see: <https://docs.qmk.fm/#/feature_layers?id=switching-and-toggling-layers>
 * -----------------------------------------------------------------------------
 */

#ifndef KEY_MACROS_H_
#define KEY_MACROS_H_

#include "quantum_keycodes.h"

// tap for space, hold for alt
// see: <https://docs.qmk.fm/#/mod_tap>
#define LALTSPC LALT_T(KC_SPC)
#define RALTSPC RALT_T(KC_SPC)

// hold for layer
// see: <https://docs.qmk.fm/#/feature_layers?id=switching-and-toggling-layers>
#define  MO_NAV MO(_NAVIGATION)

// hold for layer, tap for caps word
// see: <https://docs.qmk.fm/features/caps_word>
// use KC_F24 instead of CW_TOGG, then interrupt on tap, and set CW_TOGG
// since CW_TOGG is too high a keycode to work with layer switching.
#define CW_NAV LT(_NAVIGATION, KC_F24)
#define GAM_NAV LT(_NAVIGATION, KC_J)

// hold for layer, tap for space
#define LOW_SPC LT(_LOWER, KC_SPC)
#define RAISSPC LT(_RAISE, KC_SPC)

// set default layer
#define DF_GAME TG(_GAME)
#define DF_BASE DF(_BASE)

// shift+backspace => delete
#define DELBSPC KC_BSPC // give it a specific name so it's clear it has non-standard behavior.

// home row mods
// see: <https://precondition.github.io/home-row-mods>
// G A S C / ◆ ⎇ ⇧ ⎈
// Left-hand home row mods
#define   A_GUI LGUI_T(KC_A)
#define   S_ALT LALT_T(KC_S)
#define   D_SFT LSFT_T(KC_D)
#define   F_CTL LCTL_T(KC_F)

// Right-hand home row mods
#define QUOTGUI LGUI_T(KC_QUOT)
#define SCLNGUI LGUI_T(KC_SCLN)
#define   L_ALT LALT_T(KC_L)
#define   K_SFT LSFT_T(KC_K)
#define   J_CTL LCTL_T(KC_J)

#endif /* KEY_MACROS_H_ */
