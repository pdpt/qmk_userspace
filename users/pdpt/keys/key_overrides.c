/**
 * - \users\pdpt\keys\key_overrides.c ------------------------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  <<fileoverview>>
 * fcustom key behavior will go here.
 * things like custom keycodes, key overrides, tap dance, etc.. should all go here
 *
 * ? maybe rename to key_behavior as "key override" has a specific meaning
 * -----------------------------------------------------------------------------
 */

#include "key_macros.h"
#include "keys/layer_macros.h" // IWYU pragma: keep
#include "process_key_override.h"
// #include "layer_macros.h"
// #include "quantum.h" // IWYU pragma: keep


// shift + bckspace => delete
const key_override_t delete_key_override = ko_make_basic(MOD_MASK_SHIFT, KC_BSPC, KC_DEL);
// gui + quote => semicolon
const key_override_t gui_quot_override   = ko_make_basic(MOD_MASK_GUI, KC_QUOT, KC_SCLN);

// F24 -> caps word
const key_override_t caps_word_override = ko_make_basic(0, KC_F24, CW_TOGG);

// This globally defines all key overrides to be used
const key_override_t **key_overrides = (const key_override_t *[]){
    // &delete_key_override,
    // &caps_word_override,
    // &gui_quot_override,
    NULL // Null terminate the array of overrides!
};

// returns the tapping term given a keycode or keyrecord
uint16_t get_tapping_term(uint16_t keycode, keyrecord_t *record) {


    switch (keycode) {
        case LOW_SPC:
        case RAISSPC:
            return LAYER_SWITCH_TAPPING_TERM;
        case A_GUI:
        case S_ALT:
        case D_SFT:
        case F_CTL:
        case QUOTGUI:
        case SCLNGUI:
        case L_ALT:
        case K_SFT:
        case J_CTL:
            return HOME_ROW_MODS_TAPPING_TERM;
        default:
            return TAPPING_TERM;
    }
}
