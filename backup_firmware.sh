# ~\backup_firmware.sh
#
# Copyright 2024 Taylor Premo tpre@pm.me
# gitlab.com/pdpt
# SPDX-License-Identifier: GPL-3.0-or-later
#
# ===----------------------------------------------------------------------===
#
# \file
# custom script to move firmware to a backup folder outside the repo.
#

#!/bin/bash
# move all .uf2 and .hex files to a backup folder
day=$(date -I)
dir="X:\\archives\\backups\\qmkFirmware\\$day"

mkdir -p $dir

count=`ls -1 *.uf2 2>/dev/null | wc -l`
if [ $count != 0 ]
then
mv -b *.uf2 $dir
else
echo "no .uf2 files"
fi

count=`ls -1 *.hex 2>/dev/null | wc -l`
if [ $count != 0 ]
then
mv -b *.hex $dir
else
echo "no .hex files"
fi
