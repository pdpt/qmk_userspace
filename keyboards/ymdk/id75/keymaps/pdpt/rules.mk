################################################################################
# - \keyboards\ymdk\id75\keymaps\pdpt\rules.mk ------------------------------- #
#                                                                              #
#  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt                      #
#  SPDX-License-Identifier: GPL-3.0-or-later                                   #
# ---------------------------------------------------------------------------- #
#  <<fileoverview>>                                                            #
# ---------------------------------------------------------------------------- #
################################################################################

CUSTOM_RGB_MATRIX = yes
RGB_MATRIX_ENABLE = yes
LTO_ENABLE = yes
CONSOLE_ENABLE = yes
