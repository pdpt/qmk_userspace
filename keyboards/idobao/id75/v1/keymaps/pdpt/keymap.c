/**
 * - \keyboards\idobao\id75\v1\keymaps\pdpt\keymap.c ---------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  \file
 *  individual keymap file, should be very minimal.
 *  only inlcude things that are specific to this keyboard.
 * -----------------------------------------------------------------------------
 */

#include QMK_KEYBOARD_H

#include "users/pdpt/keys/layer_macros.h"


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
 [_BASE]       = GAME_LAYER,
 [_GAME]       = GAME_LAYER,
 [_LOWER]      = LOWER_LAYER,
 [_RAISE]      = RAISE_LAYER,
 [_NAVIGATION] = NAVIGATION_LAYER,
 [_ADJUST]     = ADJUST_LAYER,
};

#if defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)
const uint16_t PROGMEM encoder_map[][NUM_ENCODERS][NUM_DIRECTIONS] = {

};
#endif // defined(ENCODER_ENABLE) && defined(ENCODER_MAP_ENABLE)

