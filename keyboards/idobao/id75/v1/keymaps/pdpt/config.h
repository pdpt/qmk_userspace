/**
 * - \keyboards\idobao\id75\v1\keymaps\pdpt\config.h ---------------------------
 *
 *  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt
 *  SPDX-License-Identifier: GPL-3.0-or-later
 * -----------------------------------------------------------------------------
 *  \see <https://docs.qmk.fm/#/config_options?id=the-configh-file>
 * -----------------------------------------------------------------------------
 */


#ifndef CONFIG_H_
#define CONFIG_H_

#ifdef VERBOSE
    #undef VERBOSE
#endif // VERBOSE

#define RETRO_TAPPING
#define TAPPING_TERM_PER_KEY
#ifdef TAPPING_TERM
    #undef TAPPING_TERM
#endif
#define TAPPING_TERM  200

#ifdef LAYER_SWITCH_TAPPING_TERM
    #undef LAYER_SWITCH_TAPPING_TERM
#endif
#define LAYER_SWITCH_TAPPING_TERM 150

#ifdef HOME_ROW_MODS_TAPPING_TERM
    #undef HOME_ROW_MODS_TAPPING_TERM
#endif
#define HOME_ROW_MODS_TAPPING_TERM 450


#ifdef RGB_MATRIX_FRAMEBUFFER_EFFECTS
    #undef RGB_MATRIX_FRAMEBUFFER_EFFECTS
#endif

#ifdef BASE_COLOR
    #undef BASE_COLOR
#endif
#define BASE_COLOR HSV_PURPLE


#endif /* CONFIG_H_ */
