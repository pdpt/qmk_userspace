# - \keyboards\idobao\id75\v1\keymaps\pdpt\rules.mk -------------------------- #
#  Copyright 2024 Taylor Premo tpre@pm.me gitlab.com/pdpt                      #
#  SPDX-License-Identifier: GPL-3.0-or-later                                   #
# ---------------------------------------------------------------------------- #
#  <<fileoverview>>                                                            #
# ---------------------------------------------------------------------------- #

CUSTOM_RGB_MATRIX = no
RGB_MATRIX_ENABLE = no
RGBLIGHT_ENABLE = yes
LTO_ENABLE = yes
CONSOLE_ENABLE = yes
CAPS_WORD_ENABLE = no
MOUSEKEY_ENABLE = no

